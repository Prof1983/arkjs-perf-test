# Arkjs-perf-test
# 运行Arkjs-perf-test

## 介绍

自动化运行js性能测试用例并且生成daily报告，用于测试方舟builtins API性能。

## 目录

```bash
/arkcompiler/ets_runtime
│_ test                   # 模块测试用例
    |_ jsperftest         # js perf 测试代码目录
		 |-README.md      # 说明书
		 |-run_js_perf_test.sh # 测试执行shell脚本
		 |_run_js_test.py # 测试执行调用的python脚本
```

## 运行测试
说明：之前脚本命令是root用户可以自动下载配置v8环境，非root用户操作受限，所以现在统一改成需要手动下载配置v8环境。(需要更新到最新代码）
1) 脚本命令：bash run_js_perf_test.sh js_report_save_path openharmony_path d8_binary_path
   * js_report_save_path：表示报告输出的目录路径
   * openharmony_path:  openharmony根目录（out的父目录）路径
   * d8_binary_path:  v8引擎 d8 二进制的路径

- 配置v8引擎执行环境

**下载：**

```shell
# 可以在任意目录。假定当前目录在/home目录
wget https://storage.googleapis.com/chromium-%76%38/official/canary/%76%38-linux64-rel-12.0.267.zip --no-check-certificate -O v_8-linux64-rel-12.0.267.zip
unzip v_8-linux64-rel-12.0.267.zip -d v8
```

**配置：**

临时配置：

每次执行run_js_perf_test.sh脚本时传入 v8 引擎 d8 二进制的路径，比如/home/v8/d8。

永久配置：

```shell
sudo su
# 编辑/root/.bashrc文件在末尾追加以下内容
export PATH=/home/v8:$PATH
# 刷新环境变量。注意执行sh脚本依然需要传入d8的路径
source /root/.bashrc
```

**验证：**

```shell
执行： 
	/home/v8/d8 -v # 任意用户
回显：
	V8 version 12.0.267
	d8>
```

示例：
![输入图片说明](example1.png)

脚本执行完成之后的结果：  

![输入图片说明](image.png)

![输入图片说明](res.png)  

## Run on arm64 device

Download v8 ohos version:
```bash
git clone https://gitee.com/qishui7/ohcompiler-daily.git
```
See `v8/v8_lib_ohos` directory.

### Device specific option

* `--d8-device-dir <v8_ohos_lib_dir>` path to v8 binary files for ohos arm64 build. Script send v8 ohos specific files to device and run benchmarks via these files. See https://gitee.com/qishui7/ohcompiler-daily
* `--hdc-path <hdc_path>` path to specific version of `hdc` if needed. Default: `hdc` from `PATH` environment variable.
* `--platform independent_arm64` config for arm64 standalone build. See `arkcompiler/ets_runtime/test/jsperftest/config.json`
* `--taskset MASK` run benchmark processes on specific cores. It helps to get more stable results

### Example of usage
```bash
cd ark_standalone_build_root
./arkcompiler/ets_runtime/test/jsperftest/run_js_perf_test.sh \
    --d8-device-dir /home/user/ohcompiler-daily/v8/v8_lib_ohos \
    --platform independent_arm64 \
    --multiplier 10 --iterations 10 \
    --hdc-path /home/user/toolchains/hdc \
    --bench-filter=Set --exclude=Set/constructor.js:Set/iterator.js \
    --taskset 3F0 \
    /home/user/jsperf_result_dir /home/user/ark_standalone_build_root
```
